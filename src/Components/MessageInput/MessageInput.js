import React from "react";
import "./MessageInput.css";
import icon from "./paper-plane-solid.svg";
import unsetIcon from "./times-solid.svg";

class MessageInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      textarea: this.props.editMessage?.text || "",
    };
    this.onChangeHandler = this.onChangeHandler.bind(this);
    this.onSendOrUpdateMessage = this.onSendOrUpdateMessage.bind(this);
  }

  componentDidUpdate(prevProps) {
    if (prevProps.editMessage === this.props.editMessage) return;
    this.setState({
      textarea: this.props.editMessage?.text || "",
    });
  }

  onChangeHandler(e) {
    this.setState({
      textarea: e.target.value,
    });
  }

  onSendOrUpdateMessage() {
    if (this.state.textarea.length === 0) return;
    if (this.props.editMessage) {
      this.props.onUpdateMessage(this.state.textarea);
    } else {
      this.props.onSendMessage(this.state.textarea);
    }
    this.setState({
      textarea: "",
    });
  }

  render() {
    return (
      <div className="message-input">
        <textarea
          className="message-input-text"
          placeholder="Write message..."
          value={this.state.textarea}
          onChange={this.onChangeHandler}
        ></textarea>
        <div className="button-wrapper">
          <button
            className="message-input-button"
            onClick={this.onSendOrUpdateMessage}
          >
            <img className="sendArrow" src={icon} alt="arrow" />
            Send
          </button>
          {this.props.editMessage ? (
            <button
              className="message-unset-edit-message"
              onClick={this.props.onUnsetEditMessage}
            >
              <img className="unsetCross" src={unsetIcon} alt="cross" />
              Cancel
            </button>
          ) : null}
        </div>
      </div>
    );
  }
}

export default MessageInput;
