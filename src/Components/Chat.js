import React from "react";
import moment from "moment";
import { v4 as uuidv4 } from 'uuid';
import "./Chat.css";
import Header from "./Header/Header";
import MessageList from "./MessageList/MessageList";
import MessageInput from "./MessageInput/MessageInput";
import Preloader from "./Preloader/Preloader";

class Chat extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      ownerId: "owner-id",
      title: null,
      messages: [],
      isData: false,
      editMessage: null,
      canScroll: true,
    };
    this.handleToggleLikeMessage = this.handleToggleLikeMessage.bind(this);
    this.handleSendMessage = this.handleSendMessage.bind(this);
    this.handleSetEditMessage = this.handleSetEditMessage.bind(this);
    this.handleUnsetEditMessage = this.handleUnsetEditMessage.bind(this);
    this.handleUpdateMessage = this.handleUpdateMessage.bind(this);
    this.handleDeleteMessage = this.handleDeleteMessage.bind(this);
    this.handleSetCanScroll = this.handleSetCanScroll.bind(this);
  }

  componentDidMount() {
    fetch(this.props.url)
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        this.setState({
          title: "My chat",
          messages: data,
          isData: true,
        });
      });
  }

  handleSendMessage(text) {
    const time = moment.utc();
    const message = {
      id: uuidv4(),
      text: text,
      userId: this.state.ownerId,
      createdAt: time.format(),
    };
    this.setState((state) => ({
      messages: [...state.messages, message],
      canScroll: true,
    }));
  }

  handleSetEditMessage(id) {
    const message = this.state.messages.find((mes) => mes.id === id);
    this.setState({
      editMessage: { ...message },
    });
  }

  handleUnsetEditMessage() {
    this.setState({
      editMessage: null,
    });
  }

  handleUpdateMessage(text) {
    const message = { ...this.state.editMessage, text };
    this.setState((state) => ({
      editMessage: null,
      messages: state.messages.map((mes) =>
        mes.id === message.id ? message : mes
      ),
    }));
  }

  handleDeleteMessage(id) {
    this.setState((state) => ({
      messages: state.messages.filter((mes) => mes.id !== id),
    }));
  }

  handleToggleLikeMessage(id) {
    this.setState((state) => ({
      messages: state.messages.map((mes) =>
        mes.id !== id ? mes : { ...mes, isLiked: !!!mes.isLiked }
      ),
    }));
  }

  handleSetCanScroll(value) {
    this.setState({
      canScroll: value,
    });
  }

  render() {
    return (
      <div className="chat">
        {this.state.isData ? (
          <div className="chat-wrapper">
            <Header title={this.state.title} messages={this.state.messages} />
            <MessageList
              canScroll={this.state.canScroll}
              setCanScroll={this.handleSetCanScroll}
              messages={this.state.messages}
              ownerId={this.state.ownerId}
              setEditMessage={this.handleSetEditMessage}
              deleteMessage={this.handleDeleteMessage}
              toggleLikeMessage={this.handleToggleLikeMessage}
            />
            <MessageInput
              editMessage={this.state.editMessage}
              onSendMessage={this.handleSendMessage}
              onUpdateMessage={this.handleUpdateMessage}
              onUnsetEditMessage={this.handleUnsetEditMessage}
            />
          </div>
        ) : (
          <Preloader />
        )}
      </div>
    );
  }
}

export default Chat;
