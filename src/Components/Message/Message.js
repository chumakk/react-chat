import moment from "moment";
import React from "react";
import "./Message.css";

class Message extends React.Component {
  render() {
    return (
      <div className="message">
        <div className="message-container">
          <div className="message-user-name">{this.props.user}</div>
          <img
            src={this.props.avatar}
            className="message-user-avatar"
            alt="avatar"
          ></img>
          <div className="message-info">
            <div className="message-text">{this.props.text}</div>
            <div className="message-details">
              <span>sended at </span>
              <div className="message-time">
                {moment(this.props.createdAt).format("HH:mm")}
              </div>
            </div>
          </div>
          <button
            className={this.props.isLiked ? "message-liked" : "message-like"}
            onClick={() => this.props.toggleLikeMessage(this.props.id)}
          >
            <svg
              className="message-icon"
              aria-hidden="true"
              focusable="false"
              role="img"
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 512 512"
            >
              <path
                fill={this.props.isLiked ? "#e53b3b" : "#888"}
                d="M462.3 62.6C407.5 15.9 326 24.3 275.7 76.2L256 96.5l-19.7-20.3C186.1 24.3 104.5 15.9 49.7 62.6c-62.8 53.6-66.1 149.8-9.9 207.9l193.5 199.8c12.5 12.9 32.8 12.9 45.3 0l193.5-199.8c56.3-58.1 53-154.3-9.8-207.9z"
              ></path>
            </svg>
          </button>
        </div>
      </div>
    );
  }
}

export default Message;
