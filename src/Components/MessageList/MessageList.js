import React from "react";
import "./MessageList.css";
import Message from "../Message/Message";
import OwnMessage from "../OwnMessage/OwnMessage";
import moment from "moment";

class MessageList extends React.Component {
  constructor(props) {
    super(props);
    this.bottom = React.createRef();
    this.timeOfLastDivider = null;
  }

  componentDidMount() {
    if (this.props.canScroll) {
      this.scrollToBottom();
      this.props.setCanScroll(false);
    }
  }

  componentDidUpdate() {
    if (this.props.canScroll) {
      this.scrollToBottom();
      this.props.setCanScroll(false);
    }
  }

  scrollToBottom = () => {
    this.bottom.current.scrollIntoView();
  };

  messageList() {
    const elements = [];
    this.props.messages.forEach((message) => {
      if (
        !this.timeOfLastDivider ||
        moment(message.createdAt)
          .startOf("day")
          .diff(moment(this.timeOfLastDivider).startOf("day"), "days") !== 0
      ) {
        this.timeOfLastDivider = message.createdAt;
        elements.push(
          <MessageListDivider
            key={this.timeOfLastDivider}
            date={this.timeOfLastDivider}
          />
        );
      }
      if (message.userId === this.props.ownerId) {
        elements.push(
          <OwnMessage
            key={message.id}
            id={message.id}
            text={message.text}
            createdAt={message.createdAt}
            setEditMessage={this.props.setEditMessage}
            deleteMessage={this.props.deleteMessage}
          />
        );
      } else {
        elements.push(
          <Message
            key={message.id}
            id={message.id}
            user={message.user}
            avatar={message.avatar}
            text={message.text}
            createdAt={message.createdAt}
            isLiked={message.isLiked}
            toggleLikeMessage={this.props.toggleLikeMessage}
          />
        );
      }
    });

    return elements;
  }
  render() {
    this.timeOfLastDivider = null;
    return (
      <div className="message-list">
        {this.messageList()}
        <div ref={this.bottom}></div>
      </div>
    );
  }
}

class MessageListDivider extends React.Component {
  divideDate(date) {
    const currentDate = moment().startOf("day");
    const formatDate = moment(date).startOf("day");
    const diff = currentDate.diff(formatDate, "days");
    switch (diff) {
      case 0:
        return "Today";
      case 1:
        return "Yesterday";

      default:
        return formatDate.format("dddd, D MMMM");
    }
  }
  render() {
    return (
      <div className="messages-divider">
        <div className="messages-divider-line"></div>
        <div className="messages-divider-date">
          {this.divideDate(this.props.date)}
        </div>
      </div>
    );
  }
}

export default MessageList;
